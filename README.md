# ATHLIX_PRIVACYPOLICY


Privacy Policy

This Privacy Policy is an integral part of the ATHLIX app, under the responsibility of Prime Sports, Lda. The visit to the app implies, by itself, that the User voluntarily provides information through the forms available for each purpose. When accessing the app, browsers transfer some data to the server necessary for technical reasons for proper operation. For more information about cookies, please visit www.allaboutcookies.org.

Data Collection and Processing

Prime Sports, Lda is the entity responsible for collecting and processing Users' personal data. Any information that the User provides us through the app will only be collected by Prime Sports, Lda for the indicated purpose, namely, for reporting security issues or notifying a potential adverse effect of applications, comments, requests for clarification and suggestions, and may be necessary, for processing the request, to share the information with other companies, partners and regulatory authorities located in other countries. The personal data collected will be kept only for the period necessary to achieve the purposes, after which they will be deleted. Prime Sports, Lda assumes that the data were entered by the respective owner, and that they are true and accurate.

Subcontracting

Prime Sports, Lda may use specialized service providers for data processing in specific cases, in which case the User will be duly informed. Prime Sports, Lda will ensure that service providers are bound by contract and subject to a duty of confidentiality, acting in accordance with our instructions and in strict compliance with our guidelines, taking all appropriate measures to safeguard and integrity of the processed data.

Access Rights

In compliance with and to the extent provided for in the applicable legislation on the protection of personal data, the user may, at any time, exercise the rights of access, rectification, cancellation and opposition to the processing of their personal data, by written request addressed to our Data Protection Officer, by the form made available here. Prime Sports, Lda reserves the right to update the Privacy Policy of this site whenever necessary. Changes become effective after publication.